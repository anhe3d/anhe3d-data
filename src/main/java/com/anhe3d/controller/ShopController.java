package com.anhe3d.controller;

import com.anhe3d.repository.ShopRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kevinhung on 3/29/16.
 */
@Controller
@RequestMapping(path = "/shop")
public class ShopController {

    @Autowired
    ShopRepository shopRepository;

    @RequestMapping(method = RequestMethod.GET)
    public String getShopInfo(ModelMap modelMap) {

        //Shop options
        List<String> shopList = new ArrayList<>();
        shopRepository.findAll().forEach(shop -> shopList.add(shop.getShopName()));

        modelMap.addAttribute("shopList", shopList);

        return "shop";
    }

    @RequestMapping(method= RequestMethod.POST)
//    @ResponseBody
    public RedirectView redirectWithUsingRedirectView(@RequestParam("selectedList") String string1, RedirectAttributes redirectAttributes) {
        redirectAttributes.addAttribute("shopName",string1);
        return new RedirectView("menu");
    }

}
