package com.anhe3d.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by kevinhung on 3/22/16.
 */
@Controller
public class LoginController {

    @RequestMapping(value = "/login")
    public String login() {
        return "login";
    }

//    @RequestMapping(value = "/login")
//    public String login() {
//        return "login";
//    }
}
