package com.anhe3d.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by kevinhung on 3/22/16.
 */
@Controller
public class ErrorController {

    @RequestMapping("/404")
    public String errorHandler() {
        return "404";
    }
}
