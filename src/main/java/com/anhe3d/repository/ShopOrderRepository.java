package com.anhe3d.repository;

import com.anhe3d.domain.ShopOrder;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by kevinhung on 3/24/16.
 */
@Repository
public interface ShopOrderRepository extends CrudRepository<ShopOrder, Long>{
}
