package com.anhe3d.repository;

import com.anhe3d.domain.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by kevinhung on 3/21/16.
 */
@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    Employee findByLastName(String lastName);

    Employee findByFirstName(String firstName);

    Employee findByCellphone(String cellphone);

    Employee findByEmployeeId(String employeeId);

}
