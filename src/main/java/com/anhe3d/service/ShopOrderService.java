package com.anhe3d.service;

import com.anhe3d.domain.Employee;
import com.anhe3d.domain.ShopMenu;
import com.anhe3d.domain.ShopOrder;
import com.anhe3d.repository.EmployeeRepository;
import com.anhe3d.repository.ShopMenuRepository;
import com.anhe3d.repository.ShopOrderRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by kevinhung on 4/4/16.
 */
@Service
public class ShopOrderService {

    @Autowired
    ShopOrderRepository shopOrderRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    ShopMenuRepository shopMenuRepository;

    @Transactional(rollbackFor = Exception.class)
    public void addShopOrder(String employeeId, String selectedItem, int quantity, String remark) {

//        Employee employee = employeeRepository.findByEmployeeId(employeeId);
        Employee employee = employeeRepository.findByFirstName(employeeId);

        Timestamp currentTime = new Timestamp(new Date().getTime());
        String itemName = StringUtils.substringBefore(selectedItem, " ");
        ShopMenu shopMenu = shopMenuRepository.findByItemName(itemName);
        double totalPrice = shopMenu.getItemPrice()*quantity;

        ShopOrder shopOrder = new ShopOrder(employee, shopMenu, currentTime, quantity, totalPrice, remark);
        shopOrderRepository.save(shopOrder);
    }
}
