package com.anhe3d.service;

import com.anhe3d.domain.Employee;
import com.anhe3d.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by kevinhung on 4/1/16.
 */
@Service
public class EmployeeService {

    @Autowired
    EmployeeRepository employeeRepository;

    @Transactional(rollbackFor = Exception.class)
    public void addEmployee(Employee employee) throws Exception {
        if(employee != null) {
            employeeRepository.save(employee);
            throw new Exception("intend to throw the exception");
        }
    }

//    @Cacheable(
//            value = "employees",
//            key = "#id")
//    @CachePut(
//            value = "employees",
//            key = "#result.id")
    public Employee findOne(Long id) {
        return employeeRepository.findOne(id);
    }


}
