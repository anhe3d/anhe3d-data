package com.anhe3d.web.api;

import com.anhe3d.domain.Employee;
import com.anhe3d.repository.EmployeeRepository;
import com.anhe3d.service.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * Created by kevinhung on 3/22/16.
 */
@RestController
@Transactional(
        propagation = Propagation.SUPPORTS,
        readOnly = true
)
public class EmployeeController {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeController.class);

    @Autowired
    EmployeeService employeeService;

    @Autowired
    EmployeeRepository employeeRepository;




//    private static BigInteger nextId;
//    private static Map<Long, Greeting2> greetingMap;
//
//    private static Greeting2 save(Greeting2 greeting2) {
//        if (greetingMap == null) {
//            greetingMap = new HashMap<>();
//            nextId = BigInteger.ONE;
//        }
//
//        //If update...
//        if (greeting2.getId() != null) {
//            Greeting2 oldGreeting = greetingMap.get(greeting2.getId());
//            if (oldGreeting == null) {
//                return null;
//            }
//            greetingMap.remove(greeting2.getId());
//            greetingMap.put(greeting2.getId(), greeting2);
//            return greeting2;
//        }
//
//        //If create...
//        greeting2.setId(nextId.longValue());
//        nextId = nextId.add(BigInteger.ONE);
//        greetingMap.put(greeting2.getId(), greeting2);
//        return greeting2;
//    }
//    static {
//        Greeting2 g1 = new Greeting2();
//        g1.setText("Hello World");
//        save(g1);
//
//        Greeting2 g2 = new Greeting2();
//        g2.setText("Hello Kevin");
//        save(g2);
//
//    }


//    @Cacheable(
//            value = "employees",
//            key = "#id")
//    @CachePut(
//            value = "employees",
//            key = "#result.id")
//    @RequestMapping(
//            path = "/api/employees/{id}",
//            method = RequestMethod.GET,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Employee> getEmployeeById(@PathVariable("id") Long id) {
//        Employee employee = employeeService.findOne(id);
//        if (employee == null) {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
//        return new ResponseEntity<>(employee, HttpStatus.ACCEPTED);
//    }

    @Cacheable(
            value = "employees",
            key = "#employeeId")
    @CachePut(
            value = "employees",
            key = "#result")
    @RequestMapping(
            path = "/api/employees/{employeeId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> getEmployeeByEmployeeId(@PathVariable("employeeId") String employeeId) {
        Employee employee = employeeRepository.findByEmployeeId(employeeId);
        if (employee == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        logger.info("getEmployeeByEmployeeId");
        return new ResponseEntity<>(employee, HttpStatus.ACCEPTED);
    }

    @RequestMapping(
            path = "/api/employees",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<Employee>> getEmployees() {
        Collection<Employee> employees = (Collection<Employee>) employeeRepository.findAll();
        logger.info("getEmployees");
        return new ResponseEntity<>(employees, HttpStatus.OK);
    }

    @CachePut(
            value = "employees",
            key = "#result")
    @Transactional(
            propagation = Propagation.REQUIRED,
            readOnly = false
    )
    @RequestMapping(
            path = "/api/employees",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee) {
        Employee savedEmployee = employeeRepository.save(employee);
        logger.info("createEmployee");
        return new ResponseEntity<>(savedEmployee, HttpStatus.CREATED);
    }

    @CachePut(
            value = "employees",
            key = "#employee")
    @RequestMapping(
//            path = "/api/employees",
            path = "/api/employees/0033",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> updateEmployee(@RequestBody Employee employee) {
        Employee oldEmployee = employeeRepository.findByEmployeeId(employee.getEmployeeId());
        if (employee.getEmployeeId() != null) {
            if (oldEmployee == null) {
                return null;
            }
        }

        oldEmployee.setFirstName(employee.getFirstName());
        oldEmployee.setLastName(employee.getLastName());
        oldEmployee.setCellphone(employee.getCellphone());

        Employee updatedEmployee = employeeRepository.save(oldEmployee);
        if (updatedEmployee == null) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        logger.info("updateEmployee");
        return new ResponseEntity<>(updatedEmployee, HttpStatus.OK);
    }

    @CacheEvict(
           value = "employees",
            key = "#employeeId")
    @RequestMapping(
            path = "/api/employees/{employeeId}",
            method = RequestMethod.DELETE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> deleteEmployee(
            @PathVariable("employeeId") String employeeId
//            @RequestBody Employee employee
    ) {

        Employee targetEmployee = employeeRepository.findByEmployeeId(employeeId);
        if (targetEmployee == null) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        logger.info("deleteEmployee");
        employeeRepository.delete(targetEmployee);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }

    @CacheEvict(
            value = "employees",
            allEntries = true)
    public void evictCache() {

    }












//    @RequestMapping(
//            path = "/api/greetings",
//            method = RequestMethod.GET,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Collection<Greeting2>> getGreetings() {
//        Collection<Greeting2> greeting2s = greetingMap.values();
//        return new ResponseEntity<>(greeting2s, HttpStatus.OK);
//    }
//
//    @Transactional(
//            propagation = Propagation.REQUIRED,
//            readOnly = false
//    )
//    @RequestMapping(
//            path = "/api/greetings",
//            method = RequestMethod.POST,
//            consumes = MediaType.APPLICATION_JSON_VALUE,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Greeting2> createGreeting(@RequestBody Greeting2 greeting2) {
//        Greeting2 savedGreeting = save(greeting2);
//        if (savedGreeting.getId() == 4L) {
//            throw new RuntimeException("Roll me back");
//        }
//        return new ResponseEntity<>(savedGreeting, HttpStatus.CREATED);
//    }
//
//    @RequestMapping(
//            path = "/api/greetings/{Id}",
//            method = RequestMethod.PUT,
//            consumes = MediaType.APPLICATION_JSON_VALUE,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Greeting2> updateGreeting(@RequestBody Greeting2 greeting2) {
//        Greeting2 updatedGreeting = save(greeting2);
//        if (updatedGreeting == null) {
//            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//
//        return new ResponseEntity<>(updatedGreeting, HttpStatus.OK);
//    }












}
